# from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):

    # Create a URL for the request with the city and state
    url = 'https://api.pexels.com/v1/search'

    params = {
        "query": f"{city}, {state}",
        "per_page": '1',
        }

    headers = {
        'Authorization' : PEXELS_API_KEY,
        }
    response = requests.get(url, headers=headers, params=params)

    try:
        content = response.json()
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except(KeyError, IndexError, requests.exceptions.JSONDecodeError):
        return {"picture_url": None}


def get_weather(city, state):
    # print("CITY+STATE: ", city, state)

    params = {
        "appid": OPEN_WEATHER_API_KEY,
        "limit": "1",
        "q": f"{city}, {state}, US",
    }

    # geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},%20US&appid={OPEN_WEATHER_API_KEY}&limit=1"

    geo_url = f"http://api.openweathermap.org/geo/1.0/direct"

    geo_response = requests.get(geo_url, params)
    geocode = geo_response.json()
    try:

        lat = geocode[0]["lat"]
        lon = geocode[0]["lon"]
    except(KeyError, IndexError):
        return None

    weather_params = {
        "lon" : lon,
        "lat" : lat,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    weather_response = requests.get(weather_url, params=weather_params)
    weather = weather_response.json()
    temp = weather["main"]["temp"]
    description = weather["weather"][0]["description"]

    return {"temp" : temp, "description" : description}
